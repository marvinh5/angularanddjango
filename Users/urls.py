from django.conf.urls import patterns, include, url
from django.contrib import admin
from Users.views import UserGet,UserNew

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'AngularAndDjango.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
	url(r'^', UserGet.as_view()),
	url(r'^new/', UserNew.as_view())
)
