from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
import json
from django.template.response import TemplateResponse 
from django.http import JsonResponse
from django.core import serializers

# Create your views here.


from django.contrib.auth.models import User

class UserNew(View):
	def get(self, request):
		return JsonResponse({"name":"marvin"})
	def post(self, request):
	   	return JsonResponse({"name":"marvin"})

class UserGet(View):
	def get(self, request):
		return TemplateResponse(request,'Users/index.html')
	def post(self, request):
		listUsers = []
		for x in serializers.serialize("python", (User.objects.all())):
			user = {}
			for y in x['fields']:
				if y != "password":
					user[str(y)] = str(x['fields'][y]) 
			listUsers.append(user)
		return JsonResponse(listUsers, safe=False);