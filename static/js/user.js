var phonecatApp = angular.module('user',[]);
		
phonecatApp.controller('UserAdminCtrl', ['$scope','$http', function($scope, $http){
	
	$scope.headers =  ["USERNAME","NAME","LAST NAME","EMAIL"];

	//request token
	var token = document.getElementsByName("csrfmiddlewaretoken")[0].value;

	$scope.search = function()
	{
		 var request = $http({
                    method: "post",
                    url: "/users/",
                    headers:{"X-CSRFToken":token},
                    data:{

                    }
                });

		request.success(function(data)
			{
				console.log(data);
				$scope.users = data;
			});
		
		request.error(function( data, status, headers, config){
				console.log(status);
		});

		
	}
	
	$scope.clearTable = function()
	{
		$scope.users = {}
	}

	$scope.edit = function()
	{
		console.log($scope.user.edit);

		var request = $http({
			method: "post",
			url:"/users/edit/",
			headers:{"X-CSRFToken":token},
			data:{
				name:      $scope.user.edit.name,
				last_name: $scope.user.edit.last_name,
				telephone: $scope.user.edit.telephone,
				email:     $scope.user.edit.email
			}
		});
		
		request.success(function(data)
		{
			$scope.user.edit = data;
		});

		request.error(function(data)
		{
			$scope.form.error = data.error;
		});
	}
	$scope.new = function()
	{
		console.log($scope.user.new);

		var request = $http({
			method: "post",
			url:"/users/new/",
			headers:{"X-CSRFToken":token},
			data:{
				username:      $scope.user.new.username,
				first_name: $scope.user.new.first_name,
				last_name: $scope.user.new.last_name,
				telephone: $scope.user.new.telephone,
				email:     $scope.user.new.email
			}
		});
		
		request.success(function(data)
		{
			$scope.user.new = data;
		});

		request.error(function(data)
		{
			$scope.form.error = data.error;
		});
	}


	console.log($scope.users);

}]);