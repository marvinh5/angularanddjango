from django.shortcuts import render
from django.views.generic import View
from django.template.response import TemplateResponse 
# Create your views here.

class Home(View):
	def get(self, request):
		return TemplateResponse(request,'home.html',{'name':'marvin'})