from django.conf.urls import patterns, include, url
from django.contrib import admin
from AngularAndDjango.views import Home

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'AngularAndDjango.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^home/', Home.as_view()),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^users/', include('Users.urls')),
)
